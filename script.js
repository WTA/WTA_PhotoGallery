var imgArray = ["img/standalone.jpg","img/happyFox.jpg","img/octopus.jpg","img/theHealing.png",
				"img/mountains.jpg","img/road.jpg","img/stoneTortoises.jpg","img/volcano.jpg"];

//array index number.
var number;

$(document).ready(function(){

	//picked image poppup function
	$('.photo_bg img').click(function(){
		var src = $(this).attr('src');
		var id = $(this).attr('id');
		$('.photo').attr("src",src);
		//turns string id from string to integer
		number = parseInt(id);

		$( ".popup" ).animate({
    		height: "toggle"
  		}, 1000);

		//lock scrolling
		$('html, body').css({
	    	'overflow': 'hidden',
    		'height': '100%'
		});
		
		return number;
	});
	
	//next button function
	$('#next').click(function(){
		number++;
		if(number >= imgArray.length){
			number = 0;
		}
		$('.photo').attr("src",imgArray[number]);
		$( ".popup" ).animate();
		return number;
	});

	//back button function
	$('#back').click(function(){
		number--;
		if(number < 0){
			number = imgArray.length - 1;
		}
		$('.photo').attr("src",imgArray[number]);
		return number;
	});

	//close popup function
	$('.popup').click(function(){

		$( ".popup" ).animate({
    		opacity: 1,
    		height: "toggle"
  		}, 1000, function(){
			$('.popup').addClass("hide");
			$('.photo').attr('src',"");
		});

		//restore srolling
		$('html, body').css({
    		'overflow': 'auto',
		    'height': 'auto'
		});
	});
	
	//disable popup close on photo and next & back button
	$(".photo, .nav").click(function(e){
 		e.stopPropagation();
	});

});